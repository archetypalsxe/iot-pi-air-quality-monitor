# IoT Pi Air Quality Monitor

Project to build an Air Quality Monitor using a Raspberry Pi and AWS IoT

## Sensor
Decided to use the SDS011 sensor due to it being used in two of the articles I found, [readily available](https://www.amazon.com/gp/product/B08QRJSVW7) and available with a USB adapter to make connecting to a Raspberry Pi easy.

Although the [Enviro+ from Pimoroni](https://shop.pimoroni.com/products/enviro?variant=31155658457171) looks really good and has the additional features.

## Raspberry Pi Selection

I had previously decided to use a Raspberry Pi 3 Model B as I have two of them that weren't being used for much and they have a USB interface while the Zero would require an adapter. It worked *fine*... but it was slow. If I was only getting it up and going once and only restarting the service when restarting it, it would have been fine. However, having to completely restart everything everytime I made a code change and wait was a bit tedious. I started working on other projects during the time I was waiting for the Raspberry Pi 3 to perform various tasks.

I ended up switching to a Raspberry Pi 4 as it was also not being used and was a significant upgrade over the Raspberry Pi 3 in terms of how long operations would take to complete. Again, the Pi 3 would have been fine if I was more patient or wasn't restarting the service as frequently.

### Current Personal Inventory
* 2x Raspberry Pi 3 Model B
  * One of them is being used as a k3s node
  * Other one is being used for a different project
* 2x Raspberry Pi Zero W
* Raspberry Pi 4
  * Currently just being used as a k3s node
* Raspberry Pi 5
  * Currently being used as the control plane/master k3s node
  * Being used as a home server as well
* Raspberry Pi Zero
  * Currently In Use

## Software Selection
Referring to the two articles below that use the SDS011 sensor (one from Tom's Hardware, the other from Raspberry Pi.com) there are two options for a software starting point

### Raspberry Pi.com
This article uses Python with the `pyserial` module for connecting to the SDS011 module. I do like that this module is not specific to the sensor and theoretically could be used for other sensors. It then uses [`adafruit-io`](https://io.adafruit.com/) to send data readings to every 10 seconds. There doesn't appear to be any local storage. Adafruit IO does look very interesting, however it seems like it only stores data for 60 days with the paid plan.

### Tom's Hardware
This article uses a GitHub project from [rydercalmdown](https://github.com/rydercalmdown) aptly titled [Pi Air Quality Monitor](https://github.com/rydercalmdown/pi_air_quality_monitor/tree/main). Apparently there is a Python package (sds011) that already exists for reading from the SDS011 sensor that this project utilizes. The project takes readings every 60 seconds and stores them in a local Redis instance. Flask is also running for a web server that can be used to access the data "locally".

### Selection
I ended up going with the rydercalmdown GitHub project due to the local data storage and the built in web server. Ideally... I would replace the use of the `sds011` package with the `pyserial` module of the other article... but we will see if time allows. Since I am going to be replacing the non-local storage with AWS anyway... the ability to send to Adafruit IO would be replaced anyway or added to the GitHub project. The rydercalmdown project is also using Docker and Docker Compose which I am very familiar with and hoping that I could eventually move this over to a local Kubernetes environment.

The eventual plan is:
* Readings are taken every x seconds (probably 30 or 60)
* Data is stored in Redis locally
* Local data is viewable from the web server
* Every 30 minutes - an hour (???) we send the new data from the Redis server to AWS IoT
* Eventually it would be cool to either allow the web server to query AWS or to have another web server for viewing the data in AWS
  * Ideally... along with data from other sensors

## Articles Used As a Starting Point
* [Tom's Hardware](https://www.tomshardware.com/how-to/raspberry-pi-air-quality-monitor)
  * 2022
  * Uses SDS011 sensor
* [Raspberry Pi.com](https://www.raspberrypi.com/news/monitor-air-quality-with-a-raspberry-pi/)
  * 2020
  * Uses SDS011 sensor
* [hackster.io](https://www.hackster.io/david-gherghita/air-quality-monitor-using-raspberry-pi-4-sps30-and-azure-03cb42?ref=itsfoss.com)
  * 2020
  * Uses Sensirion SPS30 Sensor
* [Curiosity Saves the Planet](https://curiositysavestheplanet.com/air-quality-monitor-raspberry-pi-enviro-maker-project/)
  * 2021
  * Uses an Enviro+
    * Enviro+ has a large number of sensors including:
      * Temperature
      * Pressure
      * Humidity
      * Light
      * Analog Gas Sensor
* [Medium / Matsyuama](https://blog.inkdrop.app/how-to-build-an-air-quality-monitor-using-raspberry-pi-zero-w-c6a71b8dbb3b)
  * 2021
  * ANAVI Infrared pHAT
    * Sensors for:
      * Temperature
      * Humidity
      * Barometric
      * Light
  * MH-Z19
    * CO2 Density Sensor
* [Raspberry.com](https://projects-raspberry.com/indoor-air-quality-monitor/)
  * 2020
  * [OhmTech's uThing::VOC](https://www.hackster.io/ohmtech-io/products/uthing-voc/specs)
