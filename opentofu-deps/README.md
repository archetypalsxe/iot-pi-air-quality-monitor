# OpenTofu Deps

This is just for creating the S3 bucket and DynamoDB table that will be used to manage state for the opentofu directory. State for this directory will just be managed locally

## Usage
* From this directory
* `tofu init`
* `tofu apply`
  * Should ask you for a `bucket_name`, make sure it's unique - for all S3 buckets everywhere
* Will create a `state.hcl` file in the `opentofu` directory. Don't delete this! It's used to configure the state in the opentofu directory