# Based on: https://brendanthompson.com/posts/2021/10/dynamic-terraform-backend-configuration and https://stackoverflow.com/questions/61404008/how-to-build-up-terraform-local-file-dynamically
# Creates a config file that is used in opentofu
resource "local_file" "state_config" {
  filename = "${path.module}/../opentofu/state.hcl"
  content  = <<-EOT
    bucket = "${aws_s3_bucket.state.id}"
    key = "terraform.tfstate"
    region = "${var.region}"
    dynamodb_table = "${var.table_name}"
  EOT
}