provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Provisioner = "OpenTofu"
      repository  = "iot-pi-air-quality-monitor"
      directory   = "opentofu-deps"
      iac         = "OpenTofu"
    }
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.31.0"
    }
  }
}