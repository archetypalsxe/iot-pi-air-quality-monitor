variable "bucket_name" {
  type        = string
  description = "The name of the S3 bucket to use, it must be unique for all AWS accounts"
}

variable "table_name" {
  type        = string
  description = "The name of the DynamoDB table that is used for storing Terraform state"
  default     = "terraform-state"
}

variable "region" {
  type        = string
  description = "The AWS region to deploy resources in"
  default     = "us-east-1"
}
