resource "aws_iot_thing" "pi_server" {
  name            = "air_quality_pi"
  thing_type_name = aws_iot_thing_type.raspberry_pi.name
  attributes = {
    Project = "Air-Quality"
  }
}

resource "aws_iot_thing_type" "raspberry_pi" {
  name = "RaspberryPi"
  properties {
    description           = "Various Raspberry Pis"
    searchable_attributes = ["model", "use"]
  }
}

resource "aws_iot_policy" "default" {
  name   = "DefaultPolicy"
  policy = data.aws_iam_policy_document.default.json
}

resource "aws_iot_policy_attachment" "attachment" {
  policy = aws_iot_policy.default.name
  target = aws_iot_certificate.cert.arn
}

data "aws_iam_policy_document" "default" {
  statement {
    sid    = "Default"
    effect = "Allow"
    actions = [
      "iot:Connect",
      "iot:Publish",
      "iot:Receive",
      "iot:RetainPublish",
      "iot:Subscribe",
    ]
    # TODO Shouldn't be * resources...
    resources = ["*"]
  }
}
