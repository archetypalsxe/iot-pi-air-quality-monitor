provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Provisioner = "OpenTofu"
      repository  = "iot-pi-air-quality-monitor"
      iac         = "OpenTofu"
      directory   = "opentofu"
    }
  }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.31.0"
    }
  }

  backend "s3" {}
}