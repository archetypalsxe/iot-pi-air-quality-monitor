output "ca_pem" {
	value = data.http.root_ca.response_body
}

output "cert_pem_crt" {
  value = tls_self_signed_cert.cert.cert_pem
}

output "iot_endpoint" {
  value = data.aws_iot_endpoint.iot_endpoint.endpoint_address
}

output "key_pem_key" {
  value     = tls_private_key.key.private_key_pem
  sensitive = true
}

data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}

data "http" "root_ca" {
	url = "https://www.amazontrust.com/repository/AmazonRootCA1.pem"
}
