variable "region" {
  type        = string
  description = "The AWS region to deploy resources in"
  default     = "us-east-1"
}

variable "tls_cert_validity_hours" {
  type        = number
  description = "How many hours to keep the self signed certificate valid for"
  default     = 720 # 30 days (24 * 30)
}