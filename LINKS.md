# Links

Collection of links I used while working on this

* [AWS Docs on Creating AWS IoT Resources](https://docs.aws.amazon.com/iot/latest/developerguide/create-iot-resources.html)
* [AWS Docs on Connecting AWS IoT to a Raspberry Pi](https://docs.aws.amazon.com/iot/latest/developerguide/connecting-to-existing-device.html)
* [Blog of Managing AWS IoT Core w/ Terraform - Advanced Web](https://advancedweb.hu/how-to-manage-iot-core-resources-with-terraform/)
* [Amazon's Sample App for AWS IoT SDK](https://github.com/aws/aws-iot-device-sdk-python-v2)