# Raspberry Pi Setup

* I just set up the Raspberry Pi using the [Raspberry Pi Imager](https://www.raspberrypi.com/software/)
  * I had originally setup two of my Pis (a 4 and a 3B) with the 32 bit operating system
  * Apparently Longhorn (K8s Disk Management) does not support Arm 32 bit architecture...
    * Had to reimage both of the Pis with the 64 bit version
* Instructions on setting up the software can be found in the [software submodule](https://gitlab.com/archetypalsxe/pi-air-quality-monitor-software#installation)
